export const postResolvers = {
  Query: {
    postById: (_, { id }, { dataSources }) => {
      return dataSources.db.getPostById(id);
    },
    postByUser: (_, { userId }, { dataSources }) => {
      return dataSources.db.getPostByUser(userId);
    },
  },
  Mutation: {
    createPost: (_, { content, userId }, { dataSources }) => {
      return dataSources.db.createPost(userId, content);
    },
  },
  User: {
    posts: (_, __, { dataSources }) => {
      return dataSources.db.getPostByUser(_.id);
    },
  },
};
