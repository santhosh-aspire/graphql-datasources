export const movieResolver = {
  Query: {
    movie: (_, { id }, { dataSources }) => {
      return dataSources.movieApi.getMovie(id);
    },
    genre: (_, __, { dataSources }) => {
      return dataSources.movieApi.getGenres();
    },
  },
};
