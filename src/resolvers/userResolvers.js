export const userResolver = {
  Query: {
    user: (_, __, { dataSources }) => {
      return dataSources.db.getUsers();
    },
  },
  Mutation: {
    addUser: (_, { name, email, age }, { dataSources }) => {
      return dataSources.db.addUser(name, email, age || null);
    },
    updateUser: (_, { id, name, email, age }, { dataSources }) => {
      return dataSources.db.updateUser(id, name, email, age);
    },
  },
};
