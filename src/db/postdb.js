import { SQLDataSource } from "datasource-sql";

export class PostDB extends SQLDataSource {
  createPost(userId, content) {
    return this.knex("post")
      .insert({ userId, content })
      .then((res) => {
        console.log(res);
        return res[0];
      });
  }

  getPostByUser(userId) {
    return this.knex("post")
      .select("*")
      .where({ userId })
      .then((res) => {
        console.log(res);
        return res;
      });
  }

  getPostById(id) {
    return this.knex("post").select("*").where({ id });
    // .then((res) => console.log(res));
  }
}
