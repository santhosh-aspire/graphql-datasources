import { SQLDataSource } from "datasource-sql";

export class UserDB extends SQLDataSource {
  getUsers() {
    return this.knex.select("*").from("user");
  }
  addUser(name, email, age) {
    return this.knex
      .insert({ name, email, age })
      .into("user")
      .then((res) => {
        console.log(res);
        return res[0];
      });
  }

  updateUser(id, name, email, age) {
    return this.knex("user").update({ name, email, age }).where({ id });
  }
}
