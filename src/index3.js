import { ApolloGateway } from "@apollo/gateway";
import { ApolloServer } from "apollo-server-express";
import express from "express";

const gateway = new ApolloGateway({
  serviceList: [
    {
      name: "user",
      url: "http://localhost:3000/graphql",
    },
    {
      name: "post",
      url: "http://localhost:3001/graphql",
    },
  ],
});

const server = new ApolloServer({ gateway });
const app = express();
(async () => {
  await server.start();
  server.applyMiddleware({ app });
  app.listen(3002, () => console.log("server 3 listening at post 3002"));
})();
