import { RESTDataSource } from "apollo-datasource-rest";

export class MovieApi extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = process.env.API_URL;
  }

  willSendRequest(request) {
    request.params.set("api_key", process.env.API_KEY);
  }

  getMovie(id) {
    return this.get(`movie/${encodeURIComponent(id)}`);
  }

  getGenres() {
    return this.get(`/genre/movie/list`).then((res) => {
      return res.genres;
    });
  }
}
