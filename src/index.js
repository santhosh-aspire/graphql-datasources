import { buildFederatedSchema } from "@apollo/federation";
import { ApolloServerPluginDrainHttpServer } from "apollo-server-core";
import { ApolloServer } from "apollo-server-express";
import express from "express";
import { MovieApi } from "./api";
import { UserDB } from "./db/userdb";
import { movieResolver } from "./resolvers/movieResolvers";
import { userResolver } from "./resolvers/userResolvers";
import { movieSchema } from "./schema/movieSchema";
import { userSchema } from "./schema/userSchema";

const port = process.env.PORT;
const dbName = process.env.DB_NAME;
const user = process.env.DB_USER;
const password = process.env.DB_PASSWORD;
const host = process.env.HOST;
const dbPort = process.env.DB_PORT;
const knexConfig = {
  client: "mysql",
  connection: {
    host,
    port: dbPort,
    user,
    password,
    database: dbName,
  },
};
const db = new UserDB(knexConfig);
(async () => {
  const app = express();
  const server = new ApolloServer({
    schema: buildFederatedSchema({
      typeDefs: userSchema,
      resolvers: userResolver,
    }),
    // typeDefs: [movieSchema, userSchema],
    // resolvers: {
    //   Query: { ...movieResolver.Query, ...userResolver.Query },
    //   Mutation: { ...userResolver.Mutation },
    // },
    plugins: [ApolloServerPluginDrainHttpServer({ httpServer: app })],
    dataSources: () => {
      return {
        movieApi: new MovieApi(),
        db,
      };
    },
  });
  await server.start();
  server.applyMiddleware({ app });
  app.listen(port, () =>
    console.log(`graphql server running at http://localhost:${port}`)
  );
})();
