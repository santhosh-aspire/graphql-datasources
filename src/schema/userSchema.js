import { gql } from "apollo-server-core";

export const userSchema = gql`
  type User @key(fields: "id") {
    id: Int
    name: String
    email: String
    age: Int
  }

  type Query {
    user: [User]
  }

  type Mutation {
    addUser(name: String, email: String, age: Int): Int
    updateUser(id: Int!, name: String, email: String, age: Int): Int
  }
`;
