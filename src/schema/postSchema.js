import { gql } from "apollo-server-core";

export const postSchema = gql`
  type Post @key(fields: "id") {
    id: Int
    user: User
    content: String
  }
  extend type User @key(fields: "id") {
    id: Int @external
    posts: [Post]
  }

  type Query {
    postByUser(userId: Int): [Post]
    postById(id: Int): Post
  }
  type Mutation {
    createPost(userId: Int, content: String): Int
  }
`;
