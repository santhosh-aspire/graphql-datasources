import { gql } from "apollo-server-core";

export const movieSchema = gql`
  type Movie {
    title: String
  }

  type Genre {
    id: Int
    name: String
  }

  type Query {
    movie(id: Int): Movie
    genre: [Genre]
  }
`;
