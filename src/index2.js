import { buildFederatedSchema } from "@apollo/federation";
import { ApolloServerPluginDrainHttpServer } from "apollo-server-core";
import { ApolloServer } from "apollo-server-express";
import express from "express";
import { PostDB } from "./db/postdb";
import { postResolvers } from "./resolvers/postResolvers";
import { postSchema } from "./schema/postSchema";

const dbName = process.env.DB_NAME;
const user = process.env.DB_USER;
const password = process.env.DB_PASSWORD;
const host = process.env.HOST;
const dbPort = process.env.DB_PORT;
const knexConfig = {
  client: "mysql",
  connection: {
    host,
    port: dbPort,
    user,
    password,
    database: dbName,
  },
};
const db = new PostDB(knexConfig);
(async () => {
  const app = express();
  const server = new ApolloServer({
    schema: buildFederatedSchema({
      typeDefs: postSchema,
      resolvers: postResolvers,
    }),

    plugins: [ApolloServerPluginDrainHttpServer({ httpServer: app })],
    dataSources: () => {
      return {
        db,
      };
    },
  });
  await server.start();
  server.applyMiddleware({ app });
  app.listen(3001, () =>
    console.log(`graphql server running at http://localhost:${3001}`)
  );
})();
